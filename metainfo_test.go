package badleecher

import (
	"fmt"
	"testing"
)

func TestNewMetainfoFromFile(t *testing.T) {
	file := "testdata/archlinux.torrent"
	metainfo := NewMetainfoFromFile(file)

	name := "archlinux-2018.10.01-x86_64.iso"
	if metainfo.Info.Name != name {
		t.Errorf("got %v, want %v", metainfo.Info.Name, name)
	}

	expectedHash := "b674f2afa42d2b72b5d5dbb6965d23edaebb2364"
	computedHash := fmt.Sprintf("%x", metainfo.InfoHash)
	if computedHash != expectedHash {
		t.Errorf("got %v, want %v", computedHash, expectedHash)
	}

	expectedTotalLength := 605 // MB, not MiB
	computedTotalLength := metainfo.TotalLength / (1000 * 1000)
	if computedTotalLength != expectedTotalLength {
		t.Errorf("got %v, want %v", computedTotalLength, expectedTotalLength)
	}
}
