package badleecher

import (
	"github.com/google/go-querystring/query"
	"github.com/zeebo/bencode"
	"io/ioutil"
	"net/http"
	"sync"
	"time"
)

const eventStarted = "started"
const eventStopped = "stopped"
const eventCompleted = "completed"

type Peer struct {
	Id   *string `bencode:"peer id,omitempty"`
	Ip   string  `bencode:"ip"`
	Port int     `bencode:"port"`
}

// Parameters for tracker querying. Serializable to URI's Query String.
type TrackerRequest struct {
	InfoHash   string  `url:"info_hash"`
	PeerId     string  `url:"peer_id"`
	Port       int     `url:"port"`
	Uploaded   int     `url:"uploaded"`
	Downloaded int     `url:"downloaded"`
	Left       int     `url:"left"`
	Ip         *string `url:"ip,omitempty"`
	NumWant    *int    `url:"numwant,omitempty"`
	Event      *string `url:"event,omitempty"`
}

// Tracker response.
type TrackerResponse struct {
	FailureReason *string `bencode:"failure  reason,omitempty"`
	Interval      int     `bencode:"interval"`
	TrackerId     *string `bencode:"tracker id", omitempty`
	Complete      *int    `bencode:"complete,omitempty"`
	Incomplete    *int    `bencode:"incomplete,omitempty"`
	Peers         []Peer  `bencode:"peers"`
}

func (request *TrackerRequest) queryTracker(baseUrl string) *TrackerResponse {
	values, _ := query.Values(request)
	query := values.Encode()

	resp, err := http.Get(baseUrl + "?" + query)
	if err != nil {
		panic(err)
	}
	defer resp.Body.Close()

	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		panic(err)
	}

	response := new(TrackerResponse)
	if err = bencode.DecodeBytes(body, response); err != nil {
		panic(err)
	}

	return response
}

type TrackerAgent struct {
	peerId    string
	interval  time.Duration
	trackerId string
	Peers     chan Peer
	wg        sync.WaitGroup
	close     chan interface{}
}

func NewTrackerAgent(metainfo *Metainfo) *TrackerAgent {
	numWant := 50

	agent := new(TrackerAgent)
	agent.peerId = GeneratePeerId()
	agent.close = make(chan interface{})
	agent.Peers = make(chan Peer, numWant*2)

	event := eventStarted

	request := TrackerRequest{
		string(metainfo.InfoHash),
		agent.peerId,
		6881,
		0,
		0,
		metainfo.TotalLength,
		nil,
		&numWant,
		&event,
	}

	response := request.queryTracker(metainfo.Announce)
	for _, peer := range response.Peers {
		agent.Peers <- peer
	}

	agent.interval = time.Duration(response.Interval) * time.Second

	agent.wg.Add(1)
	go func() {
		timer := time.NewTimer(agent.interval)

		for {
			select {
			case <-agent.close:
				agent.wg.Done()
				return
			case <-timer.C:
				request.Event = nil

				response := request.queryTracker(metainfo.Announce)
				for _, peer := range response.Peers {
					agent.Peers <- peer
				}

				timer.Reset(agent.interval)
			}
		}
	}()

	return agent
}

func (agent *TrackerAgent) Shutdown() {
	agent.close <- nil
	agent.wg.Wait()
}
