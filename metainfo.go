// Bittorrent's Metainfo file parsing and representation utilities.
package badleecher

import (
	"crypto/sha1"
	"github.com/zeebo/bencode"
	"io/ioutil"
)

type file struct {
	Length int      `bencode:"length"`
	Md5sum *string  `bencode:"md5sum,omitempty"`
	Path   []string `bencode:"path"`
}

type info struct {
	Name        string  `bencode:"name"`
	PieceLength int     `bencode:"piece length"`
	Pieces      string  `bencode:"pieces"`
	Length      *int    `bencode:"length,omitmpty"`
	Md5sum      *string `bencode:"md5sum,omitempty"`
	Files       *[]file `bencode:"files,omitempty"`
}

type Metainfo struct {
	Announce     string     `bencode:"announce"`
	AnnounceList [][]string `bencode:"announce-list"`
	CreatedBy    *string    `bencode:"created by,omitempty"`
	CreationDate int        `bencode:"creation date"`
	Comment      *string    `bencode:"comment,omitempty"`
	Info         info       `bencode:"info"`
	InfoHash     []byte     `bencode:"-"`
	TotalLength  int        `bencode:"-"`
}

// Read and parse `.torrent` file
func NewMetainfoFromFile(filename string) *Metainfo {
	bytes, err := ioutil.ReadFile(filename)
	if err != nil {
		panic(err)
	}

	// Parse the metainfo file
	metainfo := new(Metainfo)
	if err = bencode.DecodeBytes(bytes, metainfo); err != nil {
		panic(err)
	}

	// Compute the info dict's hash with SHA1
	bencodedInfo, _ := bencode.EncodeBytes(metainfo.Info)
	hash := sha1.Sum(bencodedInfo)
	metainfo.InfoHash = hash[:]

	// Compute total length of all files as a whole
	if metainfo.Info.Length != nil {
		metainfo.TotalLength = *metainfo.Info.Length
	} else {
		length := 0
		for _, file := range *metainfo.Info.Files {
			length += file.Length
		}
		metainfo.TotalLength = length
	}

	return metainfo
}
