# BadLeecher 

## Introduction
BitTorrent existe depuis 2001 et a été crée par Bram Cohen. Ce protocole a été popularisé par son usage sur les sites "pirates"
a des fins de partage de fichiers. Très contreversé pendant longtemps aujourd'hui il fait partie intégrante de la technoligie
qui nous entoure. 

NetFlix, Youtube, AWS et d'autres applications utilisées tous les jours se repose sur ce protocole. Beaucoup de distributions Linux 
sont partagées par ce biais là et cela de manière totalement légale.

### Explication de BitTorrent

BitTorrent est un protocole ``peer-to-peer`` ou ``p2p`` ou chaque "Peer" rejoint un "swarm" (essaim) pour échanger des données
entre eux. Chaque "Peer" est connécté a plusieurs autres "peer" afin de pouvoir envoyer et recevoir des données.

BitTorrent exige un fichier ``.torrent``. Celui-ci vas nous fournir un certains nombre d'informations, ces informations 
vont permettre de définir comment échangées les données avec les autres "peers" et comment vérifié l'intégrité entre celle-ci.

Ce protocole offre plusieurs avantages :

* Il permet le partage de fichier de manière décentralisé. Si tout le monde joue le jeu et que donc les personnes partage 
le fichier une fois complet, celui-ci se retrouvera à plusieurs emplacements. Si le premier "seeder" disparait, un autre 
 "seeder" pourra le fournir. (Redondance) 
* Excellent pour la gestion de bande passante. Si vous télécharger un fichier depuis un serveur centrale ceci sera très 
gourmant en bande passante. (Gain en performance/ Cout -> réseau)

Lors de ce travail, nous nous sommes essentiellement servi de la documentation non-officielle, la documentation officielle
souffre d'un manque flagrant de précision et est relativement vague sur certains aspect de la spécification. 

### Plan d'ensemble de l'application

Nous avons envisagé que l'applicatif sera divisé en 3 "modules".

|      Module      | Description |
| ---------------- | ----------- |
| ```BitTorrent``` | Module qui orchestrera et aura la vue d'ensemble des pièces.           | 
| ```Tracker```    | Module qui effectue les requetes et les transmet au module ``BitTorrent`` |
| ```Peers```      | Module simple qui effectuer une connection via un socket pour telecharger les morceaux demandé par bitTorrent |

#### Diagramme

![diag_applicatif](img/diag_applicatif.png "Diagramme Applicatif")

## THP  et fichier Torrent

### Introduction

Pour effectuer notre client bitTorrent il vas nous falloir lire le fichier torrent. Puis ce connecter au Tracker. Dans 
le fichier torrent il va nous falloir :

* Le champ ``[info]``, qui posède :
  * Le nom du fichier à télécharger. Dans: ``[info][name]``.
  * Les pièces sont divisé par tranche de 20 bytes sous forme de hash (SHA1). Dans: ``[info][pieces]``.
  * La taille du fichier à télécharger. Dans: ``[info][length]``.
* L'url du tracker. Dans: ``[annouce]`` et ``[announce-list]``.

### Parsing du fichier torrent

Premièrement, il faut lire ce fichier torrent. Ceci nous permettera de savoir quoi télécharger et ou aller le chercher. 
Généralement appellé ``metainfo``, les informations contenu sont stocker dans un format appelé ``bencode``.

Voici le format bencode :

```bash
d8:announce42:http://tracker.archlinux.org:6969/announce7:comment41:Arch Linux 2018.10.01 (www.archlinux.org)
10:created by13:mktorrent 1.113:creation datei1538417175e4:
infod6:lengthi605028352e4:name31:archlinux-2018.10.01-x86_64.iso12:piece lengthi524288e
6:pieces23080:��>�,�����$�cf���%�1T0�Y�U;F	�^.��`6�t��onM�����tL�L���l�G���5m*�C�n�"ֿ~�źR�
```

#### Bencode

Bencode est un format relativement simple a decoder. Il sert a définir la structure du fichier torrent et posède plusieurs type de données.

Voici un tableau avec quelques exemples.

| Types de données           |  Identificateur | Exemple | Resultat |
| -------------------------- | --------------- | ------- | -------- |
| Les listes                 | ``l``           | ``l6:jambon7:fromagei42ee`` | ['jambon', 'fromage', 42 ] |
| Les dictionnaires          | ``d``           | ``d5:pouet6:jambon4:plop7:fromagee`` | { 'pouet': 'jambon', 'plop':'fromage'}|
| Les nombres entiers        | ``i``           | ``i42e`` | 42 |
| Les chaines de caractères  | ``12``          | ``7:fromage`` | "fromage" |

Dans ce TP il ne sera pas obligatoire de crée notre propre sérialiseur/désérialiseur. Nous utiliserons une librairie Go.

### Tracker

Mainteannt que le fichier ```.torrent``` est décodé grace à notre maitrise parfaite du ``bencode`` (merci la librairie). Il va falloir comme 
expliqué au depart se connecter au autres "peers" pour cela il va falloir récupérer une liste, cette liste est fourni par
le ``Tracker``.  Pour connaitre l'adresse du ``Tracker`` on se sert de la valeur ``announce`` récupéré dans les méta-donées.

Un Tracker est un serveur centrale qui gardant une trace des "peers" disponible. Ce n'est donc pas le ``Tracker`` qui 
distribue les données. Les données seront récupéré directement a partir de la liste fourni par le ``Tracker``.

Le ``Tracker`` fourni donc :

* La liste des "Peers".
* Les statistiques relatives aux differents "Peers".

#### Envoi de la requête

Pour pouvoir crée notre requête http il va falloir récupéré la valeur ``announce`` dans les méta-données, puis envoyé les parmètres suivants :

| Paramètres | Description |
| ---------- | ----------- |
| ``info_hash`` | Le hash en SHA1 trouvé dans les méta-données. |
| ``PeerId `` | L'identifiant unique du client |
| ``port `` | Le port sur lequel le client écoute. |
| ``upload`` | Le nombres de bytes téléversé. | 
| ``downloaded`` | Le nombre de bytes téléchargés. | 
| ``left`` | Le nombres de bytes réstant pour ce client.| 
| ``ip`` | ip du client (à vérifier)|
| ``compact`` | Si le client accèpte ou non un liste compacté contenant les peers.| 


Le ```peer_id``` doit faire une taille exact de 20bytes, 

#### Traitement de la résponse

Une fois la requête effectuer on doit traiter la réponse. La réponse est en bencode, donc il faut le decoder pour puovoir 
exploité les informations. 

La liste des "peers" se trouve dans la réponse. La liste des pairs est une chaîne binaire d'une longueur multiple de 6 octets. 
Où chaque pair consiste en une adresse IP de 4 octets et un numéro de port de 2 octets (puisque nous utilisons le format compact).

#### Diagramme temps sequence

Un simple diagramme temps sequence pour la partie Tracker. Qui consiste a effectuer la demande tous les ``n`` intervales. 
Ici ``n`` est un entier en secondes.  

![diag_tracker](img/diag_sequence_tracker.png "Diagramme temps sequence pour la partie Tracker.")

#### Implementation (GO)

La structure de donnée :
```go
// Parameters for tracker querying. Serializable to URI's Query String.
type TrackerRequest struct {
	InfoHash   string  `url:"info_hash"`
	PeerId     string  `url:"peer_id"`
	Port       int     `url:"port"`
	Uploaded   int     `url:"uploaded"`
	Downloaded int     `url:"downloaded"`
	Left       int     `url:"left"`
	Ip         *string `url:"ip,omitempty"`
	NumWant    *int    `url:"numwant,omitempty"`
	Event      *string `url:"event,omitempty"`
}
```

Generation du ``peer_id`` :

```go
package badleecher

import (
	"fmt"
	"math/rand"
)

// Generate a 20 bytes peer ID.
func GeneratePeerId() string {
	token := make([]byte, 6)
	rand.Read(token)
	return fmt.Sprintf("-BL001-A%x", token)
}
```


La methode qui effectue la requete (et reponse) :

```go
func NewTrackerAgent(metainfo *Metainfo) *TrackerAgent {
	numWant := 50

	agent := new(TrackerAgent)
	agent.peerId = GeneratePeerId()
	agent.close = make(chan interface{})
	agent.Peers = make(chan Peer, numWant*2)

	event := eventStarted

	request := TrackerRequest{
		string(metainfo.InfoHash),
		agent.peerId,
		6881,
		0,
		0,
		metainfo.TotalLength,
		nil,
		&numWant,
		&event,
	}

	response := request.queryTracker(metainfo.Announce)
	for _, peer := range response.Peers {
		agent.Peers <- peer
	}

	agent.interval = time.Duration(response.Interval) * time.Second

	agent.wg.Add(1)
	go func() {
		timer := time.NewTimer(agent.interval)

		for {
			select {
			case <-agent.close:
				agent.wg.Done()
				return
			case <-timer.C:
				request.Event = nil

				response := request.queryTracker(metainfo.Announce)
				for _, peer := range response.Peers {
					agent.Peers <- peer
				}

				timer.Reset(agent.interval)
			}
		}
	}()

	return agent
}
```

Methode qui effectue la requete et le traitement de la réponse :
```go
func NewTrackerAgent(metainfo *Metainfo) *TrackerAgent {
	numWant := 50

	agent := new(TrackerAgent)
	agent.peerId = GeneratePeerId()
	agent.close = make(chan interface{})
	agent.Peers = make(chan Peer, numWant*2)

	event := eventStarted

	request := TrackerRequest{
		string(metainfo.InfoHash),
		agent.peerId,
		6881,
		0,
		0,
		metainfo.TotalLength,
		nil,
		&numWant,
		&event,
	}

	response := request.queryTracker(metainfo.Announce)
	for _, peer := range response.Peers {
		agent.Peers <- peer
	}

	agent.interval = time.Duration(response.Interval) * time.Second

	agent.wg.Add(1)
	go func() {
		timer := time.NewTimer(agent.interval)

		for {
			select {
			case <-agent.close:
				agent.wg.Done()
				return
			case <-timer.C:
				request.Event = nil

				response := request.queryTracker(metainfo.Announce)
				for _, peer := range response.Peers {
					agent.Peers <- peer
				}

				timer.Reset(agent.interval)
			}
		}
	}()

	return agent
}
```

## Protocol PWP (Peer Wire Protocol)

### Introduction

Après avoir réçu une adresse IP et un port d'un Peer, le client doit ouvrir une connexion TCP vers le peer en question.
Une fois la connexion effectué il va faloir échangé les messages, pour cela il faut utilisé le "Peer wire protocol" 

Nous avons décomposé cela en sous partie, afin de mieux comprendre comment celui-ci fonctionne. Car il faut avant tout comprendre 
comment fonctionne le protocole si on souhaite pouvoir l'utiliser.

### La poignée de main ("HandShake")

Pour pouvoir discuter avec le/les peer(s) et telecharger les morceux de notre fichier. Pout cela il faut commencer par s'echanger un
message "handshake", il faut que le client envoye un handshake et que le peer distance lui reponde avec un handshake. Sinon rien ne se passe. 

Le client est toujours l'initiateur dans ce cas.

Les message doit avoir une taille de 49 bytes + la taille du pstr et est structuré de cette façon :
        
        [pstrlen][pstr][reserved][info_hash][peer_id]

Le message de handshake doit contenir : 

| Champs | Taille | Description | 
| ------ | ------ | ----------- |
|```peer_id``` | 20 bytes | L'id des peers (local / distant). |
| ```info_hash``` | 20 bytes | Recupéré précédement dans la metadata de notre fichier torrent. |
| ```pstr``` | (variable) | L'identifiant du protocol. |
| ```pstrlen``` | 1 byte | Longeur du champ ```pstr``` |
| ```reserved ``` | 8 bytes | Réservé... Parfois utilisé par des clients torrent.|

Les champs les plus "importants" sur lequel il faut se focus sont. 

Le ```peer_id``` contient les id du peer local et distant. Le peer id doit être d'une longeur de 20 bytes 
 
 ```info_hash``` contient un hash en ***SHA1***, on va se servir de celui-ci
 pour comparer notre hash avec avec celui du peer distant. Si le hash est le même, alors on peut commencer a telecharger nos pièces si ce n'est pas le cas
 c'est que ce n'est pas le torrent que l'on cherche alors on ferme la connexion.

### Les messages (bitfields et etats)

#### bitfields

Une fois l'echange de "handshake" effectué, le peer distant peut (il semblerait que ce soit falcutatif) nous envoyer un message de type BitField les bitfields sont utilisé
pour indiquer au peer local quelles pièces sont à disposition sur le peer distant. Nous pourrions repondre pour annoncer que 
nous avons également des pièces a disposition mais ici on ne le fera pas car le but de l'exercice n'est pas d'être un "seeder".

Voici la structure d'un message (post-handshake) :

        [length_prefix][message_ID][payload]

Voici une déscription des champs :

| Champs | Taille | Description | 
| ------ | ------ | ----------- |
| ```lengh pretifx``` | 4 bytes - big endian |Posède la longeur, codé sur 4 bytes. | 
| ```message_ID``` | 1 byte | Défini l'id pour le message courant les 4 premiers déjà attribué.|
| ```payload``` | variable | Ici c'est le payload envoyé (Data/Messages, etc...) |


Le payload d'un message bitfield contient une sequence d'octets, lorsqu'ils sont lus en binaire, chaque bit représente une partie.
Si le bit est a ```1```, cela signifie que le peer posède la pièce avec l'index associé. L'index servira a définir le 
positionnement de la pièce. Si c'est un ```0``` cela signifie que le peer ne posède pas la pièce à l'index ou il se trouve. 

Pour schématisé cette sequence d'octets, voici la structure d'un payload:

    indices :   0 1 2 3 4 5 6 7 8 9
    payload : [ 1 1 1 0 1 0 1 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 ]
    
    Ici la pièce 0, 1, 2, 4, 6, sont disponible pour le telechargement.
    
#### etats

Une fois le bitfield reçu une serie de message vont être échangé. Pour commencer chaque peer commence avec l'etat ```chocked``` 
et ```not interested```. Cela veut dire que le peer client n'est pas autorisé à demander des pièces au peer distant.

Après le handshake et le premier bitfield, on envoye un message ```interested``` au peer distant, pour indiquer que nous 
souhaitons être dans l'etat ```unchocke``` pour pouvoir demander des pièces.

Tant que le peer n'as pas recu un unchoke il ne peut pas demander une pièce au peer distant. La connexion restera donc 
dans un état "bloqué" jusqu'a que l'etat change vers unchoked ou que l'on coupe la connexion.

Pour résumé :

1. l'etat choked -> un peer en etat choked indique que le peer n'est pas autorisé à effectuer de demande de pièces.
2. l'etat unchoked -> un peer unchoked indique que le peer est autorisé à effectuer des demande de pièces.
3. l'etat interested -> Inidque que le peer est intérésé par la récupération de pièces.
4. l'etat not interested -> Indique que le peer n'est pas interessé à effectuer des demandes de pièces.

* Si on veut télécharger des pièces : il faut que le peer soit unchocked et interested. 
* Si on veut bloquer : il faut passé a l'état initial qui est choked et not interested.

On peut donc considéré que l'etat choke et unchoke comme des réglès (deny, allow) tandis que interested et not interested 
comme des intentions/volonter entre les peers. 

Voici la liste des différents états et message que l'on peut échanger après le handshake. 

| Etat | structure | description |
| ---- | --------- | ----------- |
| keep-alive | ```[len=0000]``` | C'est un message vide qui sert a maintenir la connection avec le peer. |
| choke | ```[len=0001][id=0]``` | Ne posède pas de payload. Défini un état. |
| unchoke | ```[len=0001][id=1]``` | Ne posède pas de payload. Défini un état. |
| interested | ```[len=0001][id=2]``` | Ne posède pas de payload. Défini un état. |
| not interested | ```[len=0001][id=3]```  | Ne posède pas de payload. Défini un état. |
| have | ```[len=0005][id=4][piece_index]``` | Sert a vérifier si la pièce n'est pas corrompu en comparant le hash. |
| bitfield | ```[len=0001 + bitfield_size][id=5][bitfield]``` | Le bitfield sert à annoncer quel pièce sont à diposition sur ce "peer". |
| request | ```[len=0013][id=6][[index][piece_index][length]]``` | |
| piece | ```[len=0009 + block_size][id=7][[index][begin][block]]``` | | 
| cancel | ```[len=0013][id=8][[index][begin][length]]``` | Sert à annuler la demande d'un block. | 
 
#### Diagramme temps sequence

Voici un diagramme temps sequence qui nous permet de demander une pièce :

![diag_handshake](img/diag_sequence_handshake.png "Diagrame temps sequence, prepraration a l'echange de pièces.")

Voici un diagramme temps sequence qui nous permet d'annuler un téléchargement:

![diag_cancellation](img/diag_sequence_cancel.png "Diagramme temps sequence, annulation d'un telechargement.")

### Implementation (Go)

To do... 

## Sources / Docs
* Generale:
  * [Wikipedia - Bram Cohen](https://fr.wikipedia.org/wiki/Bram_Cohen)
  * [BitTorrent Community - Specs](http://www.bittorrent.org/beps/bep_0003.html)
  * [BitTorrent Community - Specs v2](http://www.bittorrent.org/beps/bep_0052.html)
* THP :
  * [Wiki - Theory.org - MetaInfo Structure](https://wiki.theory.org/index.php/BitTorrentSpecification#Metainfo_File_Structure)
  * [Wiki - Theory.org - Tracker THP (HTTP)](https://wiki.theory.org/index.php/BitTorrentSpecification#Tracker_HTTP.2FHTTPS_Protocol)

* PWP :
  * [Wiki - Theory.org - Peer wire protocol (TCP)](https://wiki.theory.org/index.php/BitTorrentSpecification#Peer_wire_protocol_.28TCP.29)