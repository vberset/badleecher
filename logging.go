package badleecher

import (
	"github.com/kataras/golog"
)

func init() {
	// Default Output is `os.Stdout`,
	// but you can change it:
	// golog.SetOutput(os.Stderr)

	// Time Format defaults to: "2006/01/02 15:04"
	// you can change it to something else or disable it with:
	golog.SetTimeFormat("")

	// Level defaults to "info",
	// but you can change it:
	golog.SetLevel("debug")
}
