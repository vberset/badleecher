package main

import (
	bl "badleecher"
	"fmt"
	"os"
)

func main() {
	if len(os.Args) != 2 {
		fmt.Printf("usage:\t %s <torrent>\n", os.Args[0])
		os.Exit(1)
	}
	filepath := os.Args[1]

	metainfo := bl.NewMetainfoFromFile(filepath)
	tracker := bl.NewTrackerAgent(metainfo)

	for peer := range tracker.Peers {
		fmt.Println(peer)
	}
}
