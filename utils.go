package badleecher

import (
	"fmt"
	"math/rand"
)

// Generate a 20 bytes peer ID.
func GeneratePeerId() string {
	token := make([]byte, 6)
	rand.Read(token)
	return fmt.Sprintf("-BL001-A%x", token)
}
