# badleecher

## Building

Clone the the project in your `$GOPATH`. From the root of the source tree,
to install dependencies, run:

```terminal
dep ensure
```

Then, to build the the binary, run:

```terminal
go build -o badleecher cmd/badleecher/main.go
```

## Testing

To run the test suite, run:

```terminal
go test
```
