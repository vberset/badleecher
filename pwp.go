package badleecher

import (
	"bytes"
	"encoding/binary"
	"github.com/kataras/golog"
	"net"
)

type MessageID byte

const (
	Choke   int = iota
	Unchoke     = iota + 1
	Interested
	NotInterested
	Have
	Bitfield
	Request
	Piece
	Cancel
)

type Handshake struct {
	/* Todo Not sure for this one... need to check specs */
	Len      uint8
	Protocol [19]byte
	Reserved [8]uint8
	InfoHash [20]byte
	PeerID   [20]byte
}

type PeerClient struct {
	connection       *net.TCPConn
	peerName         string
	InfoHash         []byte
	localChoke       bool
	localInterested  bool
	localBitfield    []bool
	remoteChoke      bool
	remoteInterested bool
	remoteBitfield   []bool
	totalLength      int
	send             chan []byte
	stop             chan bool
	quit             chan struct{}
}

// PeerClient constructor
func NewPeerClient(peerName string, infoHash []byte, pieces int) *PeerClient {
	// Minimum requirement to start a to talk with a remote peer :
	// * PeerName / PeerID
	// * Infohash to compare
	// * Number of pieces
	pc := &PeerClient{
		peerName: peerName,
		// Set initial state
		localChoke:       true,
		localInterested:  false,
		localBitfield:    make([]bool, pieces),
		remoteChoke:      true,
		remoteInterested: false,
		remoteBitfield:   make([]bool, pieces),

		send: make(chan []byte),
		stop: make(chan bool),
		quit: make(chan struct{}),
	}
	return pc
}

/* Todo Check with Vincent how we can provide ip and port */
func startPeerClient( /*ip, port ,*/ ip net.IP, connectionChannel chan *net.TCPConn) {
	// Setup simple connection
	raddr := net.TCPAddr{IP: ip, Port: 8000}
	golog.Infof("Connecting to : %s", raddr)
	con, err := net.DialTCP("tcp4", nil, &raddr)
	if err != nil {
		golog.Fatalf("Connection failed to remote Peer : %s", err)
		return
	}
	golog.Infof("Connection established with remote Peer : %s", raddr)
	connectionChannel <- con
}

// Function used to create our message
func (pc *PeerClient) messageFactory(id int, payload interface{}) {

	// To calculate the length the message/payload must be added.
	buff := new(bytes.Buffer)
	err := binary.Write(buff, binary.BigEndian, payload)
	if err != nil {
		golog.Fatalf("Something went wrong... :%s", err)
	}

	// Add the rest of the message
	// -- 4bytes -> Length
	// -- 1bytes -> ID field
	// -- nbytes -> Payload
	payloadBuff := buff.Bytes()
	msgBuff := new(bytes.Buffer)

	// Compute the payload length
	lengthBytes := uint32(len(payloadBuff))
	err = binary.Write(msgBuff, binary.BigEndian, lengthBytes)
	if err != nil {
		golog.Fatalf("Something went wrong... : %s", err)
	}

	idBytes := uint8(id)
	err = binary.Write(msgBuff, binary.BigEndian, idBytes)
	if err != nil {
		golog.Fatalf("Something went wrong... : %s", err)
	}

	// Inject payload
	err = binary.Write(msgBuff, binary.BigEndian, payloadBuff)
	if err != nil {
		golog.Fatalf("Something went wrong... : %s", err)
	}

	// Use a channel to send message to client
	pc.send <- msgBuff.Bytes()
}

/* functions used to send messages -> choke, unchoke, interested, etc.. */

func (pc *PeerClient) sendHandshake() {}

func (pc *PeerClient) sendKeepAlive() {
	//message := make([]byte, 4)
	golog.Debugf("Sending keepalive to remote Peer: %s", pc.peerName)

	// Todo Check how to create a keepalive

}

func (pc *PeerClient) sendChoke() {
	// Create the message and provide id (type)
	go pc.messageFactory(Choke, make([]byte, 0))
	golog.Debugf("Sending 'CHOKE' to remote Peer: %s", pc.peerName)
	// Change local state to choke
	pc.localChoke = true
}

func (pc *PeerClient) sendUnchoke() {
	// Create the message and provide id (type)
	go pc.messageFactory(Unchoke, make([]byte, 0))
	golog.Debugf("Sending 'UNCHOKE' to remote Peer: %s", pc.peerName)
	// Change local state to choke
	pc.localChoke = false
}

func (pc *PeerClient) sendInterested() {
	// Create the message and provide id (type)
	go pc.messageFactory(Interested, make([]byte, 0))
	golog.Debugf("Sending 'INTERESTED' to remote Peer: %s", pc.peerName)
	// Change local state to choke
	pc.localInterested = true
}

func (pc *PeerClient) sendNotInterested() {
	// Create the message and provide id (type)
	go pc.messageFactory(NotInterested, make([]byte, 0))
	golog.Debugf("Sending 'NOTINTERSTED' to remote Peer: %s", pc.peerName)
	// Change local state to choke
	pc.localInterested = false
}

func (pc *PeerClient) sendHave(pieceIdx int) {
	// For the have we only need the piece index (provided by bitfield)
	buff := new(bytes.Buffer)
	err := binary.Write(buff, binary.BigEndian, pieceIdx)
	if err != nil {
		golog.Fatal("Have request failed... : %s", err)
	}
	golog.Debugf("Sending 'HAVE' (for piece n°: %i) to remote Peer: %s", pieceIdx, pc.peerName)
	// Change local state to choke
	pc.messageFactory(Have, buff.Bytes())
}

func (pc *PeerClient) sendBitfield() {
	//Todo: Need to find a way to convert an array of booleans to bytes =>(101111101)
}

func (pc *PeerClient) sendRequest(pieceIdx int, start int, length int) {
	// Create a bytes buffer and an create the payload with the values provided in args
	buff := new(bytes.Buffer)
	payload := []uint32{uint32(pieceIdx), uint32(start), uint32(length)}
	err := binary.Write(buff, binary.BigEndian, payload)
	if err != nil {
		golog.Fatalf("'Request' request failed... : %s", err)
	}
	golog.Debugf("Sending 'REQUEST' [piece: %i; start: %i; length: %i ] to remote Peer: %s", pieceIdx, start, length, pc.peerName)
	pc.messageFactory(Request, buff.Bytes())
}

func (pc *PeerClient) sendCancel(pieceIdx int, start int, length int) {
	// Cancel is exactly like sendRequest (except type=>id)
	buff := new(bytes.Buffer)
	payload := []uint32{uint32(pieceIdx), uint32(start), uint32(length)}
	err := binary.Write(buff, binary.BigEndian, payload)
	if err != nil {
		golog.Fatalf("Cancel request failed... : %s", err)
	}
	golog.Debugf("Sending 'CANCEL' [piece: %i; start: %i; length: %i ] to remote Peer: %s", pieceIdx, start, length, pc.peerName)
	pc.messageFactory(Cancel, buff.Bytes())

}

// Function to start connection

// Function to handle response
